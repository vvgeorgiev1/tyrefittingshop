﻿using Vehicle;

namespace TyreGarage;

/// <summary>
/// Represents a Customer coming to the tyre shop.
/// </summary>
public class Customer : ICustomer
{
    /// <summary>
    /// Creates a new instance of the <see cref="Customer" class./>
    /// </summary>
    /// <param name="id">Unique identificaiton number.</param>
    /// <param name="vehicle">The vehicle that will be served.</param>
    /// <param name="tyre">The tyres, which will be fitted on the vehicle.</param>
    public Customer(int id, IVehicle? vehicle, ITyre? tyre)
    {
        this.Id = id;
        this.Vehicle = vehicle;
        this.Tyre = tyre;
    }

    public int Id { get; private set; }
    public IVehicle? Vehicle { get; private set; }
    public ITyre? Tyre { get; private set; }
}