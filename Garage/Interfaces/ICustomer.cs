﻿using Vehicle;

namespace TyreGarage;

public interface ICustomer
{
    int Id { get; }
    IVehicle? Vehicle { get; }
    ITyre? Tyre { get; }
}

