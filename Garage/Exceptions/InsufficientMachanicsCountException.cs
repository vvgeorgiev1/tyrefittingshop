﻿using System;
namespace TyreGarage;

public class InsufficientMechanicsCountException : Exception
{
	public InsufficientMechanicsCountException(string message) : base(message)
	{
	}
}

