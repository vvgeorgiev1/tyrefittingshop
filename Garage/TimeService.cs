﻿namespace TyreGarage;

internal static class TimeService
{
    internal static string GetTimeStampString(DateTime start)
    {
        return ((DateTime.Now - start).TotalMilliseconds / 1000d).ToString("#0.000");
    }
}

