﻿namespace TyreGarage;

/// <summary>
/// Represents a worker in the tyre fitting shop.
/// </summary>
public class Mechanic
{
    internal bool IsBusy
    {
        get;
        set;
    }

    /// <summary>
    /// Represents the action of changing the tyres of a customer.
    /// </summary>
    /// <param name="customer">The <see cref="ICustomer" who needs a tyre change.</param>
    /// <param name="duration">The duration needed to change the tyres.</param>
    /// <returns></returns>
    internal async Task ChangeTyresAsync(ICustomer customer, int duration)
    {
        this.IsBusy = true;

        // Simulates the tyre fitting work process.
        await Task.Delay(duration);

        if (customer.Vehicle != null)
        {
            customer.Vehicle.TyreFitted = customer.Tyre;
        }

        this.IsBusy = false;
    }
}