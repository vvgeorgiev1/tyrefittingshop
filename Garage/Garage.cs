﻿using System.Text;
using Vehicle;

namespace TyreGarage;

/// <summary>
/// Represents the tyre fitting shop.
/// </summary>
public class Garage
{
    public static int CustomersCount = 0;

    private readonly object lockObject = new();
    private string timestamp = string.Empty;
    private readonly DateTime start;

    /// <summary>
    /// Creates a new instance of the <see cref="Garage" class./>
    /// </summary>
    /// <param name="mechanicsCount">The number of mechanics working in the shop.</param>
    public Garage(int mechanicsCount)
    {
        if (mechanicsCount <= 0)
        {
            throw new InsufficientMechanicsCountException(StringResources.InsufficientMechanicsCountExceptionMessage);
        }

        this.Mechanics = new List<Mechanic>();
        this.WaitingList = new Queue<ICustomer>();

        for (int i = 0; i < mechanicsCount; i++)
        {
            this.Mechanics.Add(new Mechanic());
        }

        this.start = DateTime.Now;
    }

    internal List<Mechanic> Mechanics { get; }
    internal Queue<ICustomer> WaitingList { get; }

    /// <summary>
    /// This method represents the action of a <see cref="Customer" coming to the shop for a tyre change./>
    /// </summary>
    /// <param name="customer">The <see cref="Customer" who needs a tyre change./></param>
    public async Task ReceiveCustomer(ICustomer customer, int duration = 0)
    {
        this.timestamp = TimeService.GetTimeStampString(this.start);
        StringBuilder welcomeMessage = new StringBuilder(String.Format(StringResources.ReceiveCustomerMessage, this.timestamp, customer.Id));
        if (customer.Vehicle != null)
        {
            welcomeMessage.Append(customer.Vehicle.ToString());
        }
        Logger.WriteLine(welcomeMessage.ToString());

        await this.ChangeTyres(customer, duration);
    }


    /// <summary>
    /// Represents the action of ordering a work to be done. It will either assign a <see cref="Mechanic"/> to do it immediately, or will enqueue it to wait for the first available mechanic.
    /// </summary>
    /// <param name="customer">The <see cref="Customer" who needs a tyre change.</param>
    private async Task ChangeTyres(ICustomer customer, int duration = 0)
    {
        if (this.Mechanics.Count < 1)
        {
            return;
        }

        Mechanic? freeMechanic = Mechanics.FirstOrDefault(m => !m.IsBusy);

        if (freeMechanic != null && this.WaitingList.Count == 0)
        {
            await this.ChangeTyresAsync(freeMechanic, customer, duration);
        }
        else
        {
            this.WaitingList.Enqueue(customer);
        }
    }

    /// <summary>
    /// Represents the action of a <see cref="Mechanic" /> changing the tyres of a <see cref="Vehicle"/>. It also implements the necessary boilerplate work --  assigning a timeframe, logging the work being done.
    /// </summary>
    /// <param name="mechanic">The <see cref="Mechanic" /> who will do the job.</param>
    /// <param name="customer"></param>
    /// <returns>The <see cref="Customer" who needs a tyre change.</returns>
    private async Task ChangeTyresAsync(Mechanic mechanic, ICustomer customer, int duration = 0)
    {
        this.timestamp = TimeService.GetTimeStampString(this.start);
        Logger.WriteLine(String.Format(StringResources.ChangeTyresMessage, this.timestamp, customer.Id, (duration / 1000f).ToString("#0.0")));

        await mechanic.ChangeTyresAsync(customer, duration);

        this.timestamp = TimeService.GetTimeStampString(this.start);
        Logger.WriteLine(String.Format(StringResources.CustomerLeftMessage, this.timestamp, customer.Id));

        ICustomer? newCustomer;
        lock (this.lockObject)
        {
            if (this.WaitingList.TryDequeue(out newCustomer))
            {
                _ = this.ChangeTyresAsync(mechanic, newCustomer, duration);
            }
        }

    }
}