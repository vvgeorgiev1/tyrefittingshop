﻿using Castle.Core.Resource;
using Moq;
using Vehicle;

namespace TyreGarage.UnitTests;

public class GarageTests
{
    [Theory]
    [InlineData(1)]
    [InlineData(5)]
    [InlineData(1000)]
    public void Should_Init_Mechanics_List(int mechanicsCount)
    {
        Garage garage = new Garage(mechanicsCount);
        Assert.NotNull(garage.Mechanics);
        Assert.Equal(Math.Max(0, mechanicsCount), garage.Mechanics.Count);
    }

    [Theory]
    [InlineData(1)]
    [InlineData(5)]
    [InlineData(1000)]
    public void Should_All_Mechanics_Be_Free_On_Init(int mechanicsCount)
    {
        Garage garage = new Garage(mechanicsCount);
        Assert.All(garage.Mechanics, mechanic => Assert.False(mechanic.IsBusy));
    }

    [Theory]
    [InlineData(0)]
    [InlineData(-1)]
    public void Should_Throw_If_Less_Than_One_Mechanic_Is_Provided(int mechanicsCount)
    {
        var customer = new Mock<ICustomer>();
        var vehicle = new Mock<IVehicle>();
        var producer = new Mock<IVehicleProducer>();
        var tyre = new Mock<ITyre>();

        vehicle.Setup(v => v.Make).Returns(producer.Object);
        vehicle.Setup(v => v.TyreFitted).Returns(tyre.Object);

        Assert.Throws<InsufficientMechanicsCountException>(() => new Garage(mechanicsCount));
    }

    [Theory]
    [InlineData(1)]
    [InlineData(2)]
    [InlineData(5)]
    [InlineData(100)]
    public void Should_Have_A_Mechanic_Busy_While_Changing_Tyres(int mechanicsCount)
    {
        int duration = 2000;
        var customer = new Mock<ICustomer>();

        var garage = new Garage(mechanicsCount);
        var t = garage.ReceiveCustomer(customer.Object, duration);

        Assert.Contains(garage.Mechanics, mechanic => mechanic.IsBusy);
    }

    [Theory]
    [InlineData(1)]
    [InlineData(2)]
    [InlineData(5)]
    [InlineData(100)]
    public async Task Should_Get_A_Mechanic_Free_After_Changing_Tyres(int mechanicsCount)
    {
        int duration = 2000;
        var customer = new Mock<ICustomer>();

        var garage = new Garage(mechanicsCount);
        var task = garage.ReceiveCustomer(customer.Object, duration);

        Assert.Contains(garage.Mechanics, mechanic => mechanic.IsBusy);

        await task;
        Assert.All(garage.Mechanics, mechanic => Assert.False(mechanic.IsBusy));
    }

    [Theory]
    [InlineData(1, 1000)]
    [InlineData(1, 2000)]
    [InlineData(1, 5000)]
    [InlineData(1, 20000)]
    [InlineData(2, 1000)]
    [InlineData(2, 2000)]
    [InlineData(2, 5000)]
    [InlineData(2, 20000)]
    [InlineData(5, 1000)]
    [InlineData(5, 2000)]
    [InlineData(5, 5000)]
    [InlineData(5, 20000)]
    [InlineData(100, 1000)]
    [InlineData(100, 2000)]
    [InlineData(100, 5000)]
    [InlineData(100, 20000)]
    public void Should_Deploy_All_Mechanics_Simultaneously(int mechanicsCount, int duration)
    {
        var garage = new Garage(mechanicsCount);

        Mock<ICustomer> customer;
        for (int i = 0; i < mechanicsCount; i++)
        {
            customer = new Mock<ICustomer>();
            _ = garage.ReceiveCustomer(customer.Object, duration);
        }

        Assert.All(garage.Mechanics, mechanic => Assert.True(mechanic.IsBusy));
    }

    [Theory]
    [InlineData(1, 1000)]
    [InlineData(1, 2000)]
    [InlineData(1, 5000)]
    [InlineData(1, 20000)]
    [InlineData(2, 1000)]
    [InlineData(2, 2000)]
    [InlineData(2, 5000)]
    [InlineData(2, 20000)]
    [InlineData(5, 1000)]
    [InlineData(5, 2000)]
    [InlineData(5, 5000)]
    [InlineData(5, 20000)]
    [InlineData(100, 1000)]
    [InlineData(100, 2000)]
    [InlineData(100, 5000)]
    [InlineData(100, 20000)]
    public void Should_Free_All_Mechanics(int mechanicsCount, int duration)
    {
        var garage = new Garage(mechanicsCount);

        Mock<ICustomer> customer;
        Task[] tasks = new Task[mechanicsCount];
        for (int i = 0; i < mechanicsCount; i++)
        {
            customer = new Mock<ICustomer>();
            customer.SetupGet<int>(c => c.Id).Returns(i);
            var task = garage.ReceiveCustomer(customer.Object, duration);
            tasks[i] = task;
        }

        Task.WaitAll(tasks);
        Assert.All(garage.Mechanics, mechanic => Assert.False(mechanic.IsBusy));
    }

    [Theory]
    [InlineData(1, 1000)]
    [InlineData(1, 2000)]
    [InlineData(1, 5000)]
    [InlineData(1, 20000)]
    [InlineData(2, 1000)]
    [InlineData(2, 2000)]
    [InlineData(2, 5000)]
    [InlineData(2, 20000)]
    [InlineData(5, 1000)]
    [InlineData(5, 2000)]
    [InlineData(5, 5000)]
    [InlineData(5, 20000)]
    [InlineData(100, 1000)]
    [InlineData(100, 2000)]
    [InlineData(100, 5000)]
    [InlineData(100, 20000)]
    public void Should_Enqueue_Customer_If_All_Mechanics_Are_Busy(int mechanicsCount, int duration)
    {
        var garage = new Garage(mechanicsCount);

        Mock<ICustomer> customer;
        for (int i = 0; i < mechanicsCount + 1; i++)
        {
            customer = new Mock<ICustomer>();
            _ = garage.ReceiveCustomer(customer.Object, duration);
        }

        Assert.Single(garage.WaitingList);
    }

    [Theory]
    [InlineData(1, 1000)]
    [InlineData(1, 2000)]
    [InlineData(1, 5000)]
    [InlineData(1, 20000)]
    [InlineData(2, 1000)]
    [InlineData(2, 2000)]
    [InlineData(2, 5000)]
    [InlineData(2, 20000)]
    [InlineData(5, 1000)]
    [InlineData(5, 2000)]
    [InlineData(5, 5000)]
    [InlineData(5, 20000)]
    [InlineData(100, 1000)]
    [InlineData(100, 2000)]
    [InlineData(100, 5000)]
    [InlineData(100, 20000)]
    public void Should_Serve_Next_Customer_When_A_Mechanic_Becomes_Free(int mechanicsCount, int duration)
    {
        var garage = new Garage(mechanicsCount);

        Mock<ICustomer> customer;
        Task[] tasks = new Task[mechanicsCount];
        for (int i = 0; i < mechanicsCount + 1; i++)
        {
            customer = new Mock<ICustomer>();
            var task = garage.ReceiveCustomer(customer.Object, duration);
            task.ContinueWith(t =>
            {
                Assert.All(garage.Mechanics, mechanic => Assert.False(mechanic.IsBusy));
                Assert.Empty(garage.WaitingList);
            }
            );
        }

        Assert.Single(garage.WaitingList);
    }
}