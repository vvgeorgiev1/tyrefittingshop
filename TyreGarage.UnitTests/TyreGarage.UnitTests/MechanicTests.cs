﻿using Moq;
using Vehicle;

namespace TyreGarage.UnitTests;

public class MechanicTests
{
    [Fact]
    public async Task Should_Fit_Tyres()
    {
        var currentTyre = MockTyre(1);
        var newTyre = MockTyre(2);
        var vehicle = MockVehicle(currentTyre);
        var customer = MockCustomer(newTyre, vehicle);

        int duration = GetDuration();
        Mechanic mechanic = new Mechanic();

        await mechanic.ChangeTyresAsync(customer.Object, duration);

        Assert.Equal(newTyre.Object, vehicle.Object.TyreFitted);
    }

    [Fact]
    public async Task Should_Not_Fit_Tyres_Before_Duration_Passed()
    {
        var currentTyre = MockTyre(1);
        var newTyre = MockTyre(2);
        var vehicle = MockVehicle(currentTyre);
        var customer = MockCustomer(newTyre, vehicle);

        int duration = GetDuration();

        Mechanic mechanic = new Mechanic();
        var task = mechanic.ChangeTyresAsync(customer.Object, duration);

        Assert.NotEqual(newTyre.Object, vehicle.Object.TyreFitted);
        await task;

        Assert.Equal(newTyre.Object, vehicle.Object.TyreFitted);
    }

    [Fact]
    public void Should_Be_Busy_While_Fitting_Tyres()
    {
        var currentTyre = MockTyre(1);
        var newTyre = MockTyre(2);
        var vehicle = MockVehicle(currentTyre);
        var customer = MockCustomer(newTyre, vehicle);

        int duration = GetDuration() * 10;

        Mechanic mechanic = new Mechanic();
        _ = mechanic.ChangeTyresAsync(customer.Object, duration);

        Assert.True(mechanic.IsBusy);
    }

    [Fact]
    public async Task Should_Become_Not_Busy_After_Fitting_Tyres()
    {
        var currentTyre = MockTyre(1);
        var newTyre = MockTyre(2);
        var vehicle = MockVehicle(currentTyre);
        var customer = MockCustomer(newTyre, vehicle);

        int duration = GetDuration();

        Mechanic mechanic = new Mechanic();

        var task = mechanic.ChangeTyresAsync(customer.Object, duration);
        Assert.True(mechanic.IsBusy);
        await task;

        Assert.False(mechanic.IsBusy);
    }

    private static Mock<ICustomer> MockCustomer(Mock<ITyre> tyre, Mock<IVehicle> vehicle)
    {
        var customer = new Mock<ICustomer>();
        customer.Setup(c => c.Tyre).Returns(tyre.Object);
        customer.Setup(c => c.Vehicle).Returns(vehicle.Object);
        return customer;
    }

    private static Mock<IVehicle> MockVehicle(Mock<ITyre> tyre)
    {
        var vehicle = new Mock<IVehicle>();
        vehicle.SetupProperty(v => v.TyreFitted, tyre.Object);
        return vehicle;
    }

    private static Mock<ITyre> MockTyre(int pressure)
    {
        var currentTyre = new Mock<ITyre>();
        currentTyre.Setup(t => t.Pressure).Returns(pressure);
        return currentTyre;
    }

    private static int GetDuration()
    {
        return 1000;
    }
}
