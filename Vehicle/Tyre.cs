﻿namespace Vehicle;

public abstract class Tyre : ITyre
{
    // For the purpose of this exercise I will not distinguish Car from Bike tyres here.
    // In a real-world situation, that would have been a question to the person providing the task.
    // I'd be happy to go ahead and implement this change if needed.

    // In addition, in my opinion, the winter tyres should be characterized by max temperature, while the summer tyres should have min temperature.
    // I have currently implemented this as per the assignment.
    public Tyre()
    {
    }

    public float Pressure { get; set; }

    public void ShowInformation()
    {
        Console.WriteLine(this.ToString());
    }

    public override string ToString()
    {
        return String.Format(StringResources.TyreInfoString, this.GetType().Name, nameof(this.Pressure), this.Pressure.ToString(StringResources.TyrePressureFormatString));
    }
}