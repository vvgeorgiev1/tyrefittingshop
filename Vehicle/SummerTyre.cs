﻿using System.Text;

namespace Vehicle;

public class SummerTyre : Tyre
{
    public const float DEFAULT_SUMMER_CAR_TYRE_PRESSURE = 2.5f;
    public const float DEFAULT_SUMMER_CAR_TYRE_TEMPERATURE = 50f;

    public SummerTyre()
    {
        this.Pressure = DEFAULT_SUMMER_CAR_TYRE_PRESSURE;
        this.MaximumTemperature = DEFAULT_SUMMER_CAR_TYRE_TEMPERATURE;
    }

    public float MaximumTemperature { get; set; }

    public override string ToString()
    {
        StringBuilder infoBuilder = new StringBuilder(base.ToString());
        infoBuilder.Append(String.Format(StringResources.TyreTemperatureSuffixString, nameof(this.MaximumTemperature), this.MaximumTemperature.ToString(StringResources.TyreTemperatureFormatString)));
        return infoBuilder.ToString();
    }
}