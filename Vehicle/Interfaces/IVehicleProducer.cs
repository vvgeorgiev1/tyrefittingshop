﻿namespace Vehicle;

public interface IVehicleProducer
{
    public string Name { get; }
    public Production Production { get; }
}