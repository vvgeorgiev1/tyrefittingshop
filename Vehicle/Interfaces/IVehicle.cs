﻿namespace Vehicle;

public interface IVehicle
{
    public ITyre? TyreFitted { get; set; }
    public IVehicleProducer Make { get; }

    public void ShowInformation();
}

