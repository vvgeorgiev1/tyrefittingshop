﻿namespace Vehicle;

public interface ITyre
{
    public float Pressure { get; set; }
    public void ShowInformation();
}

