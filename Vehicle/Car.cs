﻿namespace Vehicle;

public class Car : Vehicle
{
    public Car(IVehicleProducer make) : base(make)
    {
        if ((this.Make.Production & Production.Cars) == Production.None)
        {
            throw new UnsupportedProductionException(String.Format(StringResources.ProducerDoesNotProduceCars, make.Name));
        }

        this.TyreFitted = new SummerTyre()
        {
            Pressure = SummerTyre.DEFAULT_SUMMER_CAR_TYRE_PRESSURE,
            MaximumTemperature = SummerTyre.DEFAULT_SUMMER_CAR_TYRE_TEMPERATURE
        };
    }
}