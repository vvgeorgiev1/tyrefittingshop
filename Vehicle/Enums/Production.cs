﻿using System;
namespace Vehicle;

[Flags]
/// <summary>
/// Denotes what kind of vehicles a <see cref="VehicleProducer" /> can produce.
/// </summary>
public enum Production
{
    None = 0,
    Cars = 1,
    Bikes = 2,
    CarsAndBikes = 3
}

