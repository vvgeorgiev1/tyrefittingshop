﻿using System.Diagnostics;

namespace Vehicle;

// Abstracts the process of writing info to the output, so a single change would be needed if requirements change.
public static class Logger
{
    public static void WriteLine(string message)
    {
        Debug.WriteLine(message);
    }
}

