﻿namespace Vehicle;

public class NoTyreFittedException : Exception
{
    public NoTyreFittedException(string message) : base(message)
    {
    }
}

