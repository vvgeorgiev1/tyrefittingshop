﻿namespace Vehicle;

public class UnsupportedProductionException : Exception
{
    public UnsupportedProductionException(string message) : base(message)
    {
    }
}

