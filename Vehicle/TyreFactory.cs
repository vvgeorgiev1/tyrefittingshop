﻿namespace Vehicle;

public class TyreFactory
{
    public static Tyre? CreateTyre(string season)
    {
        switch (season.ToLower())
        {
            case "summer":
                return new SummerTyre();
            case "winter":
                return new WinterTyre();
            default:
                return null;
        }
    }
}

