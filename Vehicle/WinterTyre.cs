﻿using System.Text;

namespace Vehicle;

public class WinterTyre : Tyre
{
    public const float DEFAULT_WINTER_CAR_TYRE_PRESSURE = 2.7f;
    public const float DEFAULT_WINTER_CAR_TYRE_TEMPERATURE = -40f;
    public const float DEFAULT_WINTER_CAR_TYRE_THICKNESS = 8.0f;

    public WinterTyre()
    {
        this.Pressure = DEFAULT_WINTER_CAR_TYRE_PRESSURE;
        this.MinimumTemperature = DEFAULT_WINTER_CAR_TYRE_TEMPERATURE;
        this.Thickness = DEFAULT_WINTER_CAR_TYRE_THICKNESS;
    }

    public float MinimumTemperature { get; set; }
    public float Thickness { get; set; }

    public override string ToString()
    {
        StringBuilder infoBuilder = new StringBuilder(base.ToString());
        infoBuilder.Append(String.Format(StringResources.TyreTemperatureSuffixString, nameof(this.MinimumTemperature), this.MinimumTemperature.ToString(StringResources.TyreTemperatureFormatString)));
        infoBuilder.Append(String.Format(StringResources.TyreThicknessSuffixString, nameof(this.Thickness), this.Thickness.ToString(StringResources.TyreThicknessFormatString)));
        return infoBuilder.ToString();
    }
}

