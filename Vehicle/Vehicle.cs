﻿using System.Diagnostics;

namespace Vehicle;

public abstract class Vehicle : IVehicle
{
    public Vehicle(IVehicleProducer make)
    {
        this.Make = make;
    }

    public IVehicleProducer Make { get; private set; }
    public ITyre? TyreFitted { get; set; }

    public void ShowInformation()
    {
        Debug.WriteLine(this.ToString());
    }

    public override string ToString()
    {
        if (this.TyreFitted == null)
        {
            throw new NoTyreFittedException(StringResources.NoTyresFittedExceptionMesage);
        }

        return String.Format(StringResources.VehicleInfoString, this.GetType().Name, this.Make, this.TyreFitted.ToString());
    }

    public void FitNewTyre(string season)
    {
        this.TyreFitted = TyreFactory.CreateTyre(season);
    }
}

