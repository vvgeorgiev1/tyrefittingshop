﻿namespace Vehicle;

public class VehicleFactory
{
    public static Vehicle? CreateVehicle(string type, IVehicleProducer producer)
    {
        switch (type.ToLower())
        {
            case "car":
                return new Car(producer);
            case "bike":
                return new Bike(producer);
            default:
                return null;
        }
    }
}

