﻿namespace Vehicle;

public class Bike : Vehicle
{
    public Bike(IVehicleProducer make) : base(make)
    {
        if ((this.Make.Production & Production.Bikes) == Production.None)
        {
            throw new UnsupportedProductionException(String.Format(StringResources.ProducerDoesNotProduceBikes, make.Name));
        }
    }
}