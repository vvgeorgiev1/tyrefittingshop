﻿namespace Vehicle;

public class VehicleProducer : IVehicleProducer
{
    public static VehicleProducer Honda = new VehicleProducer("Honda", Production.CarsAndBikes);
    public static VehicleProducer Toyota = new VehicleProducer("Toyota", Production.Cars);
    public static VehicleProducer KTM = new VehicleProducer("KTM", Production.Bikes);

    internal VehicleProducer(string name, Production production)
    {
        this.Name = name;
        this.Production = production;
    }

    public string Name { get; }
    public Production Production { get; }

    public override string ToString()
    {
        return this.Name;
    }
}