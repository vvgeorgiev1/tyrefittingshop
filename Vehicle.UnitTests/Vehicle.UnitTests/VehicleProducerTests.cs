﻿using System;
namespace Vehicle.UnitTests;

public class VehicleProducerTests
{
	[Theory]
	[InlineData(Production.None)]
	[InlineData(Production.Cars)]
	[InlineData(Production.Bikes)]
	[InlineData(Production.CarsAndBikes)]
	public void Info_String_Should_Contain_Name(Production production)
	{
		string name = "Name";
		var vechicleProducer = new VehicleProducer(name, production);
		string info = vechicleProducer.ToString();
		Assert.Contains(name, info);
	}

	[Theory]
    [InlineData(Production.None)]
    [InlineData(Production.Cars)]
    [InlineData(Production.Bikes)]
    [InlineData(Production.CarsAndBikes)]
    public void Shoud_Create_With_Empty_Name(Production production)
	{
		string name = string.Empty;
		var vehicleProducer = new VehicleProducer(name, production);
		string info = vehicleProducer.ToString();
	}
}

