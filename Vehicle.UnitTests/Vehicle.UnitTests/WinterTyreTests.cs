﻿namespace Vehicle.UnitTests;

public class WinterTyreTests 
{
    [Fact]
    public void Should_Have_Default_Pressure()
    {
        var tyre = new WinterTyre();
        Assert.Equal(2.7f, tyre.Pressure, 0.001);
    }

    [Fact]
    public void Should_Have_Default_MinimumTemperature()
    {
        var tyre = new WinterTyre();
        Assert.Equal(-40f, tyre.MinimumTemperature, 0.001);
    }

    [Fact]
    public void Should_Have_Default_Thickness()
    {
        var tyre = new WinterTyre();
        Assert.Equal(8.0f, tyre.Thickness, 0.001);
    }

    [Fact]
    public void Info_String_Should_Contain_WinterTyre()
    {
        var tyre = new WinterTyre();
        string info = tyre.ToString();
        Assert.Contains(nameof(WinterTyre), info);
    }

    [Fact]
    public void Info_String_Should_Contain_PressureString()
    {
        var tyre = new WinterTyre();
        string info = tyre.ToString();
        Assert.Contains(nameof(WinterTyre.Pressure), info);
    }

    [Fact]
    public void Info_String_Should_Contain_PressureValue()
    {
        var tyre = new WinterTyre();
        string info = tyre.ToString();
        string pressureString = tyre.Pressure.ToString(global::Vehicle.StringResources.TyrePressureFormatString);
        Assert.Contains(pressureString, info);
    }

    [Fact]
    public void Info_String_Should_Contain_MinimumTemperature_String()
    {
        var tyre = new WinterTyre();
        string info = tyre.ToString();
        Assert.Contains(nameof(WinterTyre.MinimumTemperature), info);
    }

    [Fact]
    public void Info_String_Should_Contain_MinimumTemperature_Value()
    {
        var tyre = new WinterTyre();
        string info = tyre.ToString();
        string pressureString = tyre.MinimumTemperature.ToString(StringResources.TyreTemperatureFormatString);
        Assert.Contains(pressureString, info);
    }

    [Fact]
    public void Info_String_Should_Contain_ThicknessString()
    {
        var tyre = new WinterTyre();
        string info = tyre.ToString();
        Assert.Contains(nameof(WinterTyre.Thickness), info);
    }

    [Fact]
    public void Info_String_Should_Contain_ThicknessValue()
    {
        var tyre = new WinterTyre();
        string info = tyre.ToString();
        string pressureString = tyre.Thickness.ToString(StringResources.TyreThicknessFormatString);
        Assert.Contains(pressureString, info);
    }
}
