﻿namespace Vehicle.UnitTests;

public class SummerTyreTests
{
    [Fact]
    public void Should_Have_Default_Pressure()
    {
        var tyre = new SummerTyre();
        Assert.Equal(2.5f, tyre.Pressure, 0.001);
    }

    [Fact]
    public void Should_Have_Default_MaximumTemperature()
    {
        var tyre = new SummerTyre();
        Assert.Equal(50f, tyre.MaximumTemperature, 0.001);
    }

    [Fact]
    public void Info_String_Should_Contain_SummerTyre()
    {
        var tyre = new SummerTyre();
        string info = tyre.ToString();
        Assert.Contains(nameof(SummerTyre), info);
    }

    [Fact]
    public void Info_String_Should_Contain_Pressure_String()
    {
        var tyre = new SummerTyre();
        string info = tyre.ToString();
        Assert.Contains(nameof(SummerTyre.Pressure), info);
    }

    [Fact]
    public void Info_String_Should_Contain_Pressure_Value()
    {
        var tyre = new SummerTyre();
        string info = tyre.ToString();
        string pressureString = tyre.Pressure.ToString(StringResources.TyrePressureFormatString);
        Assert.Contains(pressureString, info);
    }

    [Fact]
    public void Info_String_Should_Contain_MaximumTemperature_String()
    {
        var tyre = new SummerTyre();
        string info = tyre.ToString();
        Assert.Contains(nameof(SummerTyre.MaximumTemperature), info);
    }

    [Fact]
    public void Info_String_Should_Contain_MaximumTemperature_Value()
    {
        var tyre = new SummerTyre();
        string info = tyre.ToString();
        string pressureString = tyre.MaximumTemperature.ToString(StringResources.TyreTemperatureFormatString);
        Assert.Contains(pressureString, info);
    }
}
