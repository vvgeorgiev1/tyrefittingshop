﻿using Moq;

namespace Vehicle.UnitTests;

public class CarTests
{
	[Fact]
	public void Car_Should_Have_Summer_Tyres()
	{
        var vehicleProducer = new Mock<IVehicleProducer>();
        vehicleProducer.SetupGet<Production>(vp => vp.Production).Returns(Production.Cars);
        var vehicle = new Car(vehicleProducer.Object);
		Assert.NotNull(vehicle.TyreFitted);
		Assert.IsType<SummerTyre>(vehicle.TyreFitted);
	}

    [Fact]
    public void Info_String_Should_Contain_Car()
    {
        var vehicleProducer = new Mock<IVehicleProducer>();
        vehicleProducer.SetupGet<Production>(vp => vp.Production).Returns(Production.Cars);
        var vehicle = new Car(vehicleProducer.Object);
        Assert.Contains(typeof(Car).Name, vehicle.ToString());
    }

    [Fact]
    public void Info_String_Should_Contain_Producer_Name()
    {
        string producerName = "ProducerName";
        var vehicleProducer = new Mock<IVehicleProducer>();
        vehicleProducer.SetupGet<Production>(vp => vp.Production).Returns(Production.Cars);
        vehicleProducer.Setup(vp => vp.ToString()).Returns(producerName);
        var vehicle = new Car(vehicleProducer.Object);
        Assert.Contains(producerName, vehicle.ToString());
    }

    [Fact]
    public void Info_String_Should_Contain_Tyres_Type()
    {
        var vehicleProducer = new Mock<IVehicleProducer>();
        vehicleProducer.SetupGet<Production>(vp => vp.Production).Returns(Production.Cars);
        var vehicle = new Car(vehicleProducer.Object);
        Assert.Contains(typeof(SummerTyre).Name, vehicle.ToString());
    }

    [Theory]
    [InlineData("summer", typeof(SummerTyre))]
    [InlineData("winter", typeof(WinterTyre))]
    public void Should_Fit_New_Tyres(string season, Type type)
    {
        var vehicleProducer = new Mock<IVehicleProducer>();
        vehicleProducer.SetupGet<Production>(vp => vp.Production).Returns(Production.Cars);
        var vehicle = new Car(vehicleProducer.Object);
        vehicle.FitNewTyre(season);
        Assert.IsType(type, vehicle.TyreFitted);
    }

    [Fact]
    public void Should_Throw_If_Create_Car_By_Bikes_Only_Producer()
    {
        var vehicleProducer = new Mock<IVehicleProducer>();
        vehicleProducer.SetupGet<Production>(vp => vp.Production).Returns(Production.Bikes);
        Assert.Throws<UnsupportedProductionException>(() => new Car(vehicleProducer.Object));
    }

    [Fact]
    public void Should_Throw_If_Create_Car_By_Non_Producer()
    {
        var vehicleProducer = new Mock<IVehicleProducer>();
        vehicleProducer.SetupGet<Production>(vp => vp.Production).Returns(Production.None);
        Assert.Throws<UnsupportedProductionException>(() => new Car(vehicleProducer.Object));
    }
}

