﻿using System;
using Moq;

namespace Vehicle.UnitTests;

public class BikesTests
{
    [Fact]
    public void Bike_Should_Not_Have_Tyres()
    {
        var vehicleProducer = new Mock<IVehicleProducer>();
        vehicleProducer.SetupGet<Production>(vp => vp.Production).Returns(Production.Bikes);
        var vehicle = new Bike(vehicleProducer.Object);
        Assert.Null(vehicle.TyreFitted);
    }

    [Fact]
    public void Info_String_Should_Contain_Bike()
    {
        var vehicleProducer = new Mock<IVehicleProducer>();
        vehicleProducer.SetupGet<Production>(vp => vp.Production).Returns(Production.Bikes);

        var tyre = new Mock<SummerTyre>();
        tyre.Setup(t => t.ToString()).Returns(typeof(SummerTyre).Name);

        var vehicle = new Bike(vehicleProducer.Object);
        vehicle.TyreFitted = tyre.Object;

        Assert.Contains(typeof(Bike).Name, vehicle.ToString());
    }

    [Fact]
    public void Info_String_Should_Contain_Producer_Name()
    {
        string producerName = "ProducerName";
        var vehicleProducer = new Mock<IVehicleProducer>();
        vehicleProducer.SetupGet<Production>(vp => vp.Production).Returns(Production.Bikes);
        vehicleProducer.Setup(vp => vp.ToString()).Returns(producerName);

        var tyre = new Mock<SummerTyre>();
        tyre.Setup(t => t.ToString()).Returns(typeof(SummerTyre).Name);

        var vehicle = new Bike(vehicleProducer.Object);
        vehicle.TyreFitted = tyre.Object;

        Assert.Contains(producerName, vehicle.ToString());
    }

    [Fact]
    public void Info_String_Should_Contain_Tyres_Type()
    {
        var vehicleProducer = new Mock<IVehicleProducer>();
        vehicleProducer.SetupGet<Production>(vp => vp.Production).Returns(Production.Bikes);

        var tyre = new Mock<SummerTyre>();
        tyre.Setup(t => t.ToString()).Returns(typeof(SummerTyre).Name);

        var vehicle = new Bike(vehicleProducer.Object);
        vehicle.TyreFitted = tyre.Object;
        Assert.Contains(typeof(SummerTyre).Name, vehicle.ToString());
    }

    [Theory]
    [InlineData("summer", typeof(SummerTyre))]
    [InlineData("winter", typeof(WinterTyre))]
    public void Should_Fit_New_Tyres(string season, Type type)
    {
        var vehicleProducer = new Mock<IVehicleProducer>();
        vehicleProducer.SetupGet<Production>(vp => vp.Production).Returns(Production.Bikes);
        var vehicle = new Bike(vehicleProducer.Object);
        vehicle.FitNewTyre(season);
        Assert.IsType(type, vehicle.TyreFitted);
    }

    [Fact]
    public void Should_Throw_If_Create_Bike_By_Cars_Only_Producer()
    {
        var vehicleProducer = new Mock<IVehicleProducer>();
        vehicleProducer.SetupGet<Production>(vp => vp.Production).Returns(Production.Cars);
        Assert.Throws<UnsupportedProductionException>(() => new Bike(vehicleProducer.Object));
    }

    [Fact]
    public void Should_Throw_If_Create_Bike_By_Non_Producer()
    {
        var vehicleProducer = new Mock<IVehicleProducer>();
        vehicleProducer.SetupGet<Production>(vp => vp.Production).Returns(Production.None);
        Assert.Throws<UnsupportedProductionException>(() => new Bike(vehicleProducer.Object));
    }

    [Fact]
    public void Should_Throw_If_No_Tyres_Fitted_And_Info_String_Requested()
    {
        var vehicleProducer = new Mock<IVehicleProducer>();
        vehicleProducer.SetupGet<Production>(vp => vp.Production).Returns(Production.Bikes);
        var vehicle = new Bike(vehicleProducer.Object);

        Assert.Throws<NoTyreFittedException>(() => vehicle.ToString());
    }
}

