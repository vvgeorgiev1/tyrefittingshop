﻿using Moq;

namespace Vehicle.UnitTests;

public class VehicleFactoryTests
{
    [Fact]
    public void Should_Create_Car_By_Cars_Producer()
    {
        var vehicleProducer = new Mock<IVehicleProducer>();
        vehicleProducer.SetupGet<Production>(vp => vp.Production).Returns(Production.Cars);
        IVehicle? vehicle = VehicleFactory.CreateVehicle("car", vehicleProducer.Object);
        Assert.NotNull(vehicle);
        Assert.IsType<Car>(vehicle);
    }

    [Fact]
    public void Should_Create_Car_By_Cars_And_Bikes_Producer()
    {
        var vehicleProducer = new Mock<IVehicleProducer>();
        vehicleProducer.SetupGet<Production>(vp => vp.Production).Returns(Production.CarsAndBikes);
        IVehicle? vehicle = VehicleFactory.CreateVehicle("car", vehicleProducer.Object);
        Assert.NotNull(vehicle);
        Assert.IsType<Car>(vehicle);
    }

    [Fact]
    public void Should_Create_Bike_By_Bikes_Producer()
    {
        var vehicleProducer = new Mock<IVehicleProducer>();
        vehicleProducer.SetupGet<Production>(vp => vp.Production).Returns(Production.Bikes);
        IVehicle? vehicle = VehicleFactory.CreateVehicle("bike", vehicleProducer.Object);
        Assert.NotNull(vehicle);
        Assert.IsType<Bike>(vehicle);
    }

    [Fact]
    public void Should_Create_Bike_By_Cars_And_Bikes_Producer()
    {
        var vehicleProducer = new Mock<IVehicleProducer>();
        vehicleProducer.SetupGet<Production>(vp => vp.Production).Returns(Production.CarsAndBikes);
        IVehicle? vehicle = VehicleFactory.CreateVehicle("bike", vehicleProducer.Object);
        Assert.NotNull(vehicle);
        Assert.IsType<Bike>(vehicle);
    }

    [Theory]
    [InlineData(Production.None)]
    [InlineData(Production.Cars)]
    [InlineData(Production.Bikes)]
    [InlineData(Production.CarsAndBikes)]
    public void Should_Not_Create_Vehicle_By_Unknow_Type(Production production)
    {
        var vehicleProducer = new Mock<IVehicleProducer>();
        vehicleProducer.SetupGet<Production>(vp => vp.Production).Returns(production);
        IVehicle? vehicle = VehicleFactory.CreateVehicle("bus", vehicleProducer.Object);
        Assert.Null(vehicle);
    }
}