﻿using System;
namespace Vehicle.UnitTests;

public class TyreFactoryTests
{
	[Fact]
	public void Should_Create_Summer_Tyre()
	{
		var tyre = TyreFactory.CreateTyre("summer");
		Assert.NotNull(tyre);
		Assert.IsType<SummerTyre>(tyre);
	}

    [Fact]
    public void Should_Create_Winter_Tyre()
    {
        var tyre = TyreFactory.CreateTyre("winter");
        Assert.NotNull(tyre);
        Assert.IsType<WinterTyre>(tyre);
    }

    [Fact]
    public void Should_Not_Create_Tyre_For_Unknown_Type()
    {
        var tyre = TyreFactory.CreateTyre("spring");
        Assert.Null(tyre);
    }
}

