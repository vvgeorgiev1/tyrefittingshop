﻿using Vehicle;
using TyreGarage;

public class Program
{
    private const int MAX_CUSTOMER_DELAY = 1000;
    private const int DEFAULT_MECHANICS_COUNT = 4;
    private const int DEFAULT_CUSTOMERS_LIMIT = 100;

    async public static Task Main(string[] args)
    {
        int mechanicsCount = DEFAULT_MECHANICS_COUNT;
        int customersLimit = DEFAULT_CUSTOMERS_LIMIT;

        int argsLength = args.Length;
        if (argsLength > 0)
        {
            mechanicsCount = int.Parse(args[0]);
        }

        if (argsLength > 1)
        {
            customersLimit = int.Parse(args[1]);
        }

        Garage garage = new Garage(mechanicsCount);
        Random random = new Random();

        for (int i = 0; i < customersLimit; i++)
        {
            Garage.CustomersCount++;
            _ = garage.ReceiveCustomer(new Customer(Garage.CustomersCount, GetVehicle(), GetTyre()), GetDuration());
            await Task.Delay(random.Next(MAX_CUSTOMER_DELAY));
        }

        Console.ReadKey();
    }

    #region Helper members to fill the shop with customers
    private static Vehicle.Vehicle? GetVehicle()
    {
        bool dividesByThree = Garage.CustomersCount % 3 == 0;
        string season = dividesByThree ? "summer" : "winter";

        string vehicleType;
        if (Garage.CustomersCount % 2 == 0)
        {
            vehicleType = "car";
        }
        else
        {
            vehicleType = "bike";
        }

        VehicleProducer? vehicleProducer = GetProducer(vehicleType);
        Vehicle.Vehicle? vehicle;
        vehicle = VehicleFactory.CreateVehicle(vehicleType, vehicleProducer!);

        if (vehicle != null)
        {
            vehicle.FitNewTyre(season);
        }

        return vehicle;
    }

    private static Tyre GetTyre()
    {
        if (Garage.CustomersCount % 5 == 0)
        {
            return new SummerTyre();
        }
        else
        {
            return new WinterTyre();
        }
    }

    private static VehicleProducer? GetProducer(string vehicleType)
    {
        switch (vehicleType.ToLower())
        {
            case "car":
                return DateTime.Now.Millisecond % 2 == 0 ? VehicleProducer.Honda : VehicleProducer.Toyota;
            case "bike":
                return DateTime.Now.Millisecond % 2 == 0 ? VehicleProducer.Honda : VehicleProducer.KTM;
            default:
                return null;
        }
    }

    private const int MIN_TYRECHANGE_DURATION = 2000;
    private const int MAX_TYRECHANGE_DURATION = 5000;
    static Random random = new Random(DateTime.Now.Millisecond);

    static int GetDuration()
    {
        return (int)Math.Floor(GetRandomDuration(MIN_TYRECHANGE_DURATION, MAX_TYRECHANGE_DURATION));
    }

    internal static float GetRandomDuration(int min, int max)
    {
        float r = random.NextSingle();
        return min + (max - min) * r;
    }
    #endregion
}